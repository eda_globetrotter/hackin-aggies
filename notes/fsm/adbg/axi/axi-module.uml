'	This PlantUML script is written by Zhiyang Ong for drawing FSM of advanced debug unit's AXI module.

'	The MIT License (MIT)

'	Copyright (c) <2017> Zhiyang Ong

'	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

'	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

'	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

'	Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"		Don't compromise my computing accounts. You have been warned.



/'
 ' Drawing FSM of advanced debug unit's AXI module.
 '/
@startuml

'	Dummy state transition, or declaration of initial node to declare the figure as a FSM, rather than a sequence diagram.
[*] --> idle

idle --> Rbegin : module_cmd && module_select_i && update_dr_i && burst_read 
idle --> Wready : module_cmd && module_select_i && update_dr_i && burst_write
idle --> idle : else

Rbegin --> idle : word_count_zero
Rbegin --> Rready : else	` ~word_count_zero

Rready --> Rstatus : module_select_i && capture_dr_i
Rready --> Rready : else

Rstatus --> idle : update_dr_i
Rstatus --> Rburst : biu_ready
Rstatus --> Rstatus : else

Rburst --> idle : update_dr_i
Rburst --> Rcrc : bit_count_max && word_count_zero
Rburst --> Rburst: else 

Rcrc --> idle : update_dr_i
Rcrc --> Rcrc : else		` ~update_dr_i

Wready --> idle : word_count_zero
Wready --> Wwait : module_select_i && capture_dr_i
Wready --> Wready : else

Wwait --> idle : update_dr_i
Wwait --> Wburst : module_select_i && data_register_i[63]
Wwait --> Wwait : else

Wburst --> idle : update_dr_i
Wburst --> Wcrc : bit_count_max && word_count_zero
Wburst --> Wburst : bit_count_max && !word_count_zero
Wburst --> Wburst : else

Wstatus --> idle : update_dr_i
Wstatus --> Wcrc : word_count_zero
Wstatus --> Wburst : else

Wcrc --> idle : update_dr_i
Wcrc --> Wmatch : bit_count_32
Wcrc --> Wcrc : else

Wmatch --> idle : update_dr_i
Wmatch --> Wmatch : else

@enduml
